// src/lib.rs -- The root library crate of this project, has a giant function
//               for parsing and interpreting the file, should really split it
//               up.  Also has a few small helper functions.
// Copyright (C) 2019 Uko Koknevics
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
// OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

extern crate ansi_term;
extern crate rand;

mod error;
mod expr;
mod var;

use ansi_term::Colour;
use rand::Rng;
use std::collections::HashMap;
use std::error::Error;
use std::fs;
use std::io;
use std::io::Write;
use std::thread;
use std::time::Duration;
use var::{FloatVar, IntVar, NoneVar, StringVar, Var};

pub fn choose(choices: Vec<String>, colour: &Colour) -> Result<String, Box<dyn Error>> {
	loop {
		print!("\n{}", colour.paint("> "));
		io::stdout().flush()?;

		let mut line = String::new();
		io::stdin().read_line(&mut line)?;
		let tmp = line.trim().parse::<usize>();
		if let Ok(tmp) = tmp {
			if tmp > choices.len() || tmp <= 0 {
				println!("{}", colour.paint("Choice out of range!"));
				continue;
			}

			let tmp = tmp - 1;
			if choices[tmp] == "0" {
				println!("{}", colour.paint("Invalid choice!"));
				continue;
			}

			return Ok(choices[tmp].clone());
		} else {
			println!("{}", colour.paint("You must use numbers!"));
		}
	}
}

pub fn cls() {
	print!("{}[2J", 27 as char);
	print!("{}[H", 27 as char);
}

pub fn slowtype(text: &str, speed: u64) -> Result<(), Box<dyn Error>> {
	if speed == 0 {
		println!("{}", text);
		return Ok(());
	}

	for c in text.chars() {
		print!("{}", c);
		io::stdout().flush()?;
		if !c.is_whitespace() {
			let dur = Duration::from_millis(speed);
			thread::sleep(dur);
		}
	}

	println!();
	io::stdout().flush()?;

	Ok(())
}

pub fn run(filename: &str) -> Result<(), Box<dyn Error>> {
	let contents = fs::read_to_string(filename)?;
	let mut credits: Option<String> = None;
	let mut prefix = String::new();
	let mut comment = false;
	let mut print = false;
	let mut writeln = false;
	let mut jmp = false;
	let mut if_skip_depth = 0;
	let mut endif_skip_depth = 0;
	let mut label = String::new();
	let mut colour = Colour::Yellow;
	let mut speed = 0;
	let mut choice_count = 0;
	let mut random_choice_count = 0;
	let mut choices = Vec::new();
	let mut vars = HashMap::<String, Box<dyn Var>>::new();

	for line in contents.split('\n') {
		let mut line: String = line.to_string();

		if choice_count > 0 {
			let line = line.trim().to_lowercase();
			choices.push(line);
			choice_count -= 1;
			if choice_count == 0 {
				jmp = true;
				label = choose(choices, &colour)?;
				choices = Vec::new();
			}

			continue;
		}

		if random_choice_count > 0 {
			let line = line.trim().to_lowercase();
			choices.push(line);
			random_choice_count -= 1;
			if random_choice_count == 0 {
				jmp = true;
				label = choices[rand::thread_rng().gen_range(0..choices.len())].clone();
				choices = Vec::new();
			}

			continue;
		}

		if writeln {
			slowtype(&colour.paint(line).to_string(), speed)?;
			writeln = false;
			continue;
		}

		if print {
			if line.trim().to_lowercase().starts_with("printend") {
				print = false;
			} else {
				slowtype(&colour.paint(line).to_string(), speed)?;
			}

			continue;
		}

		if comment {
			let end = line.find('}');
			if let Some(end) = end {
				comment = false;
				line = prefix + &line[(end + 1)..];
				prefix = String::new();
			} else {
				continue;
			}
		}

		while line.contains('{') {
			let beg = line.find('{');
			if let Some(beg) = beg {
				prefix = prefix + &line[..beg];

				let end = line.find('}');
				if let Some(end) = end {
					line = prefix + &line[(end + 1)..];
					prefix = String::new();
				} else {
					comment = true;
					// This breaks out of `while line.contains('{')`
					break;
				}
			}
		}

		if comment {
			continue;
		}

		let line = line.trim();

		if line.len() == 0 {
			continue;
		}

		let tokens: Vec<&str> = line.split_whitespace().collect();
		let word = tokens[0].to_lowercase();

		if jmp && word == "label" {
			if tokens.len() < 2 {
				return make_err!(
					"'label' command expects 1 argument, {} were specified",
					tokens.len() - 1
				);
			}

			let lbl = tokens[1].to_lowercase();

			if lbl == label {
				jmp = false;
				label = String::new();
			}

			continue;
		}

		if jmp {
			continue;
		}

		if if_skip_depth != 0 {
			if word == "if" {
				if_skip_depth += 1;
			} else if word == "end" {
				if_skip_depth -= 1;
			}

			continue;
		}

		if endif_skip_depth != 0 {
			if word == "if" {
				if_skip_depth += 1;
			} else if word == "endif" {
				endif_skip_depth -= 1;
			}

			continue;
		}

		match &word[..] {
			"badend" => match credits {
				None => return Ok(()),
				Some(s) => return run(&s),
			},
			"choice" => {
				if tokens.len() < 2 {
					return make_err!(
						"'Choice' command expects 1 argument, {} were specified",
						tokens.len() - 1
					);
				}

				let tmp = tokens[1].parse();

				if let Ok(c) = tmp {
					choice_count = c;
				} else {
					return make_err!(
						"'Choice' command expects a numeric argument, got '{}' instead",
						tokens[1]
					);
				}
			}
			"cls" => cls(),
			"color" => {
				if tokens.len() < 2 {
					return make_err!(
						"'Color' command expects 1 argument, {} were specified",
						tokens.len() - 1
					);
				}

				let tmp = tokens[1].parse();

				if let Ok(c) = tmp {
					colour = Colour::Fixed(c);
				} else {
					return make_err!(
						"'Color' command expects a numeric argument, got '{}' instead",
						tokens[1]
					);
				}
			}
			"credits" => {
				if tokens.len() < 2 {
					return make_err!(
						"'Credits' command expects 1 argument, {} were specified",
						tokens.len() - 1
					);
				}

				match credits {
					None => credits = Some(tokens[1].to_string() + ".txt"),
					Some(s) => {
						return make_err!(
							"Credits were already specified:\nOld credits: '{}'\nNew credits: '{}'",
							s,
							tokens[1]
						);
					}
				}
			}
			"end" => endif_skip_depth += 1,
			"endif" => (),
			"goodend" => {
				slowtype(
					&colour.paint("CONGRATULATIONS!  YOU WON!").to_string(),
					speed,
				)?;
				match credits {
					None => return Ok(()),
					Some(s) => return run(&s),
				}
			}
			"goto" => {
				if tokens.len() < 2 {
					return make_err!(
						"'GoTo' command expects 1 argument, {} were specified",
						tokens.len() - 1
					);
				}

				jmp = true;
				label = tokens[1].to_lowercase();
			}
			"if" => {
				if tokens.len() < 3 {
					return make_err!(
						"'If' command expects at least 2 arguments, {} were specified",
						tokens.len() - 1
					);
				}

				let mut it = tokens.iter();
				it.next();
				match expr::parse_expr(it, &vars) {
					Ok(v) => match v.get_bool() {
						None => {
							return make_err!(
								"'If' command expression should return bool, got {} instead",
								v.get_type()
							);
						}
						Some(b) => {
							if !b {
								if_skip_depth = 1;
							}
						}
					},
					Err(e) => return Err(e),
				}
			}
			// TODO: Could give an error if it doesn't have enough arguments
			// for a correct label
			"label" => (),
			"linuxish" | "windowish" => (),
			"print" => print = true,
			"random" => {
				if tokens.len() < 2 {
					return make_err!(
						"'Random' command expects 1 argument, {} were specified",
						tokens.len() - 1
					);
				}

				let tmp = tokens[1].parse();
				if let Ok(tmp) = tmp {
					random_choice_count = tmp;
				} else {
					return make_err!(
						"'Random' command expects a numeric argument, got '{}' instead",
						tokens[1]
					);
				}
			}
			"sleep" => {
				if tokens.len() < 2 {
					return make_err!(
						"'Sleep' command expects 1 argument, {} were specified",
						tokens.len() - 1
					);
				}

				let tmp = tokens[1].parse();

				if let Ok(tmp) = tmp {
					thread::sleep(Duration::from_millis(tmp));
				} else {
					return make_err!(
						"'Sleep' command expects a numeric argument, got '{}' instead",
						tokens[1]
					);
				}
			}
			"slowtype" => {
				if tokens.len() < 2 {
					return make_err!(
						"'SlowType' command expects at least 1 argument, {} were specified",
						tokens.len() - 1
					);
				}

				if tokens[1] == "0" {
					speed = 0;
				} else if tokens[1] == "1" {
					if tokens.len() < 3 {
						return make_err!(
							"'SlowType 1' command expects two arguments, {} were specified",
							tokens.len() - 1
						);
					}

					let tmp = tokens[2].parse();
					if let Ok(tmp) = tmp {
						speed = tmp;
					} else {
						return make_err!(
							"'SlowType 1' command expects a numeric argument for its second argument, got '{}' instead",
							tokens[2]
						);
					}
				} else {
					return make_err!(
						"'SlowType' command expects '1' or '0' for its first argument, got '{}' instead",
						tokens[1]
					);
				}
			}
			"var" => {
				if tokens.len() < 2 {
					return make_err!(
						"'Var' command expects 1 argument, {} were specified",
						tokens.len() - 1
					);
				}

				vars.insert(tokens[1].to_lowercase(), Box::new(NoneVar()));
			}
			"w8" => {
				println!("\n{}", colour.paint("Press enter to continue..."));

				let mut tmp = String::new();
				io::stdin().read_line(&mut tmp)?;
			}
			"writeln" => writeln = true,
			_ => match vars.get(&word) {
				None => return make_err!("Unknown command: '{}'", word),
				Some(_) => {
					if tokens.len() < 2 {
						return make_err!(
							"'{}' aka variable assignment expects 1 argument, {} were specified",
							word,
							tokens.len() - 1
						);
					}

					if let Ok(i) = tokens[1].parse() {
						vars.insert(word, Box::new(IntVar(i)));
					} else if let Ok(f) = tokens[1].parse() {
						vars.insert(word, Box::new(FloatVar(f)));
					} else {
						// TODO: Make string variables nicer
						let mut it = tokens.iter();
						it.next();
						let mut s = String::new();
						for t in it {
							s = s + t + " ";
						}

						vars.insert(word, Box::new(StringVar(s.trim().to_string())));
					}
				}
			},
		};
	}

	make_err!("Unexpected end of file")
}
