// src/main.rs -- The root binary crate of this project, has a small CLI arg
//                reader and then gives it all to the `run` in src/lib.rs.
// Copyright (C) 2019 Uko Koknevics
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
// OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

extern crate clap;

use clap::{App, Arg};
use std::process;
use story_rwandan;

fn main() {
	let matches = App::new("Story Rwandan")
		.version(clap::crate_version!())
		.author(clap::crate_authors!("\n"))
		.about(clap::crate_description!())
		.arg(
			Arg::with_name("INPUT")
				.help("Sets the script file to read")
				.required(true)
				.index(1),
		)
		.get_matches();

	let filename = matches.value_of("INPUT").unwrap();
	println!("Using script file: {}", filename);

	if let Err(e) = story_rwandan::run(filename) {
		println!("Error: {}", e);

		process::exit(1);
	}
}
