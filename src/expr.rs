// src/expr.rs -- Parsing expressions (postfix)
// Copyright (C) 2019 Uko Koknevics
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
// OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use super::var::{BoolVar, FloatVar, IntVar, Ordering, Var, VarType};
use crate::make_err;
use std::collections::HashMap;
use std::error::Error;

pub fn parse_expr<'a, I>(
	it: I,
	vars: &HashMap<String, Box<dyn Var>>,
) -> Result<Box<dyn Var>, Box<dyn Error>>
where
	I: Iterator<Item = &'a &'a str>,
{
	let mut it = it;
	let tmp = it.next();
	if let Some(tmp) = tmp {
		if *tmp != "(" {
			return make_err!("Expression must start with '(', got '{}' instead", tmp);
		}
	} else {
		return make_err!("Empty expression");
	}

	let mut stack = Vec::<Box<dyn Var>>::new();

	for token in it {
		match &token.to_lowercase()[..] {
			")" => break,
			"+" => {
				let b = stack.pop();
				let a = stack.pop();
				match a {
					None => return make_err!("'+' operator requires two values on the stack"),
					Some(a) => {
						if let Some(b) = b {
							match a.add(b) {
								Ok(tmp) => stack.push(tmp),
								Err(e) => return Err(e),
							}
						}
					}
				}
			}
			"-" => {
				let b = stack.pop();
				let a = stack.pop();
				match a {
					None => return make_err!("'-' operator requires two values on the stack"),
					Some(a) => {
						if let Some(b) = b {
							match a.sub(b) {
								Ok(tmp) => stack.push(tmp),
								Err(e) => return Err(e),
							}
						}
					}
				}
			}
			"=" => {
				let b = stack.pop();
				let a = stack.pop();
				match a {
					None => return make_err!("'=' operator requires two values on the stack"),
					Some(a) => {
						if let Some(b) = b {
							match a.cmp(b) {
								Ok(tmp) => stack.push(Box::new(BoolVar(tmp == Ordering::EQ))),
								Err(e) => return Err(e),
							}
						}
					}
				}
			}
			"<>" | "!=" => {
				let b = stack.pop();
				let a = stack.pop();
				match a {
					None => return make_err!("'!=' operator requires two values on the stack"),
					Some(a) => {
						if let Some(b) = b {
							match a.cmp(b) {
								Ok(tmp) => stack.push(Box::new(BoolVar(tmp != Ordering::EQ))),
								Err(e) => return Err(e),
							}
						}
					}
				}
			}
			">" => {
				let b = stack.pop();
				let a = stack.pop();
				match a {
					None => return make_err!("'>' operator requires two values on the stack"),
					Some(a) => {
						if let Some(b) = b {
							if a.get_type() == VarType::BOOL || b.get_type() == VarType::BOOL {
								return make_err!("Bool values can't be compared with '>'");
							}

							match a.cmp(b) {
								Ok(tmp) => stack.push(Box::new(BoolVar(tmp == Ordering::GT))),
								Err(e) => return Err(e),
							}
						}
					}
				}
			}
			"<" => {
				let b = stack.pop();
				let a = stack.pop();
				match a {
					None => return make_err!("'<' operator requires two values on the stack"),
					Some(a) => {
						if let Some(b) = b {
							if a.get_type() == VarType::BOOL || b.get_type() == VarType::BOOL {
								return make_err!("Bool values can't be compared with '<'");
							}

							match a.cmp(b) {
								Ok(tmp) => stack.push(Box::new(BoolVar(tmp == Ordering::LT))),
								Err(e) => return Err(e),
							}
						}
					}
				}
			}
			">=" | "=>" => {
				let b = stack.pop();
				let a = stack.pop();
				match a {
					None => return make_err!("'>=' operator requires two values on the stack"),
					Some(a) => {
						if let Some(b) = b {
							if a.get_type() == VarType::BOOL || b.get_type() == VarType::BOOL {
								return make_err!("Bool values can't be compared with '>='");
							}

							match a.cmp(b) {
								Ok(tmp) => stack.push(Box::new(BoolVar(
									tmp == Ordering::GT || tmp == Ordering::EQ,
								))),
								Err(e) => return Err(e),
							}
						}
					}
				}
			}
			"<=" | "=<" => {
				let b = stack.pop();
				let a = stack.pop();
				match a {
					None => return make_err!("'<=' operator requires two values on the stack"),
					Some(a) => {
						if let Some(b) = b {
							if a.get_type() == VarType::BOOL || b.get_type() == VarType::BOOL {
								return make_err!("Bool values can't be compared with '<='");
							}

							match a.cmp(b) {
								Ok(tmp) => stack.push(Box::new(BoolVar(
									tmp == Ordering::LT || tmp == Ordering::EQ,
								))),
								Err(e) => return Err(e),
							}
						}
					}
				}
			}
			"tofloat" => match stack.pop() {
				None => return make_err!("'ToFloat' operator requires a value on the stack"),
				Some(a) => match a.to_float() {
					Ok(tmp) => stack.push(Box::new(tmp)),
					Err(e) => return Err(e),
				},
			},
			"toint" => match stack.pop() {
				None => return make_err!("'ToInt' operator requires a value on the stack"),
				Some(a) => match a.to_int() {
					Ok(tmp) => stack.push(Box::new(tmp)),
					Err(e) => return Err(e),
				},
			},
			"tostring" => match stack.pop() {
				None => return make_err!("'ToString' operator requires a value on the stack"),
				Some(a) => match a.to_string() {
					Ok(tmp) => stack.push(Box::new(tmp)),
					Err(e) => return Err(e),
				},
			},
			_ => {
				if let Ok(i) = token.parse() {
					stack.push(Box::new(IntVar(i)));
				} else if let Ok(f) = token.parse() {
					stack.push(Box::new(FloatVar(f)));
				} else {
					match vars.get(*token) {
						None => return make_err!("Unknown operator '{}'", token),
						Some(v) => stack.push(v.copy()),
					}
				}
			}
		}
	}

	if stack.len() != 1 {
		make_err!(
			"Expr needs one value on stack at the end, have {} instead",
			stack.len()
		)
	} else {
		Ok(stack[0].copy())
	}
}
