// src/var/mod.rs -- Defines the Var trait, None, Bool types.
// Copyright (C) 2019 Uko Koknevics
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
// OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

mod float_var;
mod int_var;
mod string_var;

pub use float_var::FloatVar;
pub use int_var::IntVar;
pub use string_var::StringVar;

use crate::make_err;
use std::error::Error;
use std::fmt::Error as FmtError;
use std::fmt::{Display, Formatter};

#[derive(PartialEq)]
pub enum VarType {
	INT,
	FLT,
	STR,
	NONE,
	BOOL,
}

impl Display for VarType {
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> {
		match self {
			VarType::INT => f.write_str("Int"),
			VarType::FLT => f.write_str("Float"),
			VarType::STR => f.write_str("String"),
			VarType::NONE => f.write_str("None"),
			VarType::BOOL => f.write_str("Bool"),
		}?;

		Ok(())
	}
}

// NEQ exists for bool comparisons
#[derive(PartialEq)]
pub enum Ordering {
	LT,
	GT,
	EQ,
	NEQ,
}

pub trait Var {
	fn copy(&self) -> Box<dyn Var>;
	fn display(&self) -> String;
	fn get_type(&self) -> VarType;

	fn get_int(&self) -> Option<i64> {
		None
	}

	fn get_float(&self) -> Option<f64> {
		None
	}

	fn get_string(&self) -> Option<&str> {
		None
	}

	fn get_bool(&self) -> Option<bool> {
		None
	}

	fn add(&self, _other: Box<dyn Var>) -> Result<Box<dyn Var>, Box<dyn Error>> {
		make_err!(
			"'+' not implemented for {}, try casting to something else",
			self.get_type()
		)
	}

	fn cmp(&self, _other: Box<dyn Var>) -> Result<Ordering, Box<dyn Error>> {
		make_err!(
			"comparison is not implemented for {}, try casting to something else",
			self.get_type()
		)
	}

	fn sub(&self, _other: Box<dyn Var>) -> Result<Box<dyn Var>, Box<dyn Error>> {
		make_err!(
			"'-' not implemented for {}, try casting to something else",
			self.get_type()
		)
	}

	fn to_float(&self) -> Result<FloatVar, Box<dyn Error>> {
		make_err!(
			"'ToFloat' not implemented for {}, try casting to something else",
			self.get_type()
		)
	}

	fn to_int(&self) -> Result<IntVar, Box<dyn Error>> {
		make_err!(
			"'ToInt' not implemented for {}, try casting to something else",
			self.get_type()
		)
	}

	fn to_string(&self) -> Result<StringVar, Box<dyn Error>> {
		make_err!(
			"'ToString' not implemented for {}, try casting to something else",
			self.get_type()
		)
	}
}

pub struct NoneVar();

impl Var for NoneVar {
	fn copy(&self) -> Box<dyn Var> {
		Box::new(NoneVar())
	}

	fn display(&self) -> String {
		String::from("None")
	}

	fn get_type(&self) -> VarType {
		VarType::NONE
	}
}

pub struct BoolVar(pub bool);

impl Var for BoolVar {
	fn copy(&self) -> Box<dyn Var> {
		Box::new(BoolVar(self.0))
	}

	fn display(&self) -> String {
		format!("{}", self.0)
	}

	fn get_type(&self) -> VarType {
		VarType::BOOL
	}

	fn get_bool(&self) -> Option<bool> {
		Some(self.0)
	}

	fn cmp(&self, other: Box<dyn Var>) -> Result<Ordering, Box<dyn Error>> {
		if let Some(b) = other.get_bool() {
			if self.0 == b {
				Ok(Ordering::EQ)
			} else {
				Ok(Ordering::NEQ)
			}
		} else {
			make_err!("Can't compare a Bool with a {}.", other.get_type())
		}
	}

	fn to_string(&self) -> Result<StringVar, Box<dyn Error>> {
		Ok(StringVar(format!("{}", self.0)))
	}
}
