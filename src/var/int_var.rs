// src/var/int_var.rs -- Defines the Int type
// Copyright (C) 2019 Uko Koknevics
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
// OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use super::float_var::FloatVar;
use super::string_var::StringVar;
use super::{Ordering, Var, VarType};
use crate::make_err;
use std::error::Error;

pub struct IntVar(pub i64);

impl Var for IntVar {
	fn copy(&self) -> Box<dyn Var> {
		Box::new(IntVar(self.0))
	}

	fn display(&self) -> String {
		format!("{}", self.0)
	}

	fn get_type(&self) -> VarType {
		VarType::INT
	}

	fn get_int(&self) -> Option<i64> {
		Some(self.0)
	}

	fn add(&self, other: Box<dyn Var>) -> Result<Box<dyn Var>, Box<dyn Error>> {
		if let Some(i) = other.get_int() {
			Ok(Box::new(IntVar(self.0 + i)))
		} else {
			make_err!(
				"Can't use '+' on an Int and a {}, try casting with 'ToInt'",
				other.get_type()
			)
		}
	}

	fn cmp(&self, other: Box<dyn Var>) -> Result<Ordering, Box<dyn Error>> {
		if let Some(i) = other.get_int() {
			if self.0 > i {
				Ok(Ordering::GT)
			} else if self.0 < i {
				Ok(Ordering::LT)
			} else {
				Ok(Ordering::EQ)
			}
		} else {
			make_err!(
				"Can't compare an Int with a {}, try casting with 'ToInt'",
				other.get_type()
			)
		}
	}

	fn sub(&self, other: Box<dyn Var>) -> Result<Box<dyn Var>, Box<dyn Error>> {
		if let Some(i) = other.get_int() {
			Ok(Box::new(IntVar(self.0 - i)))
		} else {
			make_err!(
				"Can't use '-' on an Int and a {}, try casting with 'ToInt'",
				other.get_type()
			)
		}
	}

	fn to_float(&self) -> Result<FloatVar, Box<dyn Error>> {
		Ok(FloatVar(self.0 as f64))
	}

	fn to_int(&self) -> Result<IntVar, Box<dyn Error>> {
		Ok(IntVar(self.0))
	}

	fn to_string(&self) -> Result<StringVar, Box<dyn Error>> {
		Ok(StringVar(format!("{}", self.0)))
	}
}
