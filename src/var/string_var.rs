// src/var/string_var.rs -- Defines the String type
// Copyright (C) 2019 Uko Koknevics
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
// OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use super::float_var::FloatVar;
use super::int_var::IntVar;
use super::{Ordering, Var, VarType};
use crate::make_err;
use std::error::Error;

pub struct StringVar(pub String);

impl Var for StringVar {
	fn copy(&self) -> Box<dyn Var> {
		Box::new(StringVar(self.0.clone()))
	}

	fn display(&self) -> String {
		format!("'{}'", self.0)
	}

	fn get_type(&self) -> VarType {
		VarType::STR
	}

	fn get_string(&self) -> Option<&str> {
		Some(&self.0)
	}

	fn add(&self, other: Box<dyn Var>) -> Result<Box<dyn Var>, Box<dyn Error>> {
		if let Some(s) = other.get_string() {
			Ok(Box::new(StringVar(self.0.clone() + s)))
		} else {
			make_err!(
				"Can't use '+' on a String and a {}, try casting with 'ToString'",
				other.get_type()
			)
		}
	}

	fn cmp(&self, other: Box<dyn Var>) -> Result<Ordering, Box<dyn Error>> {
		if let Some(s) = other.get_string() {
			let s = s.to_string();
			if self.0 > s {
				Ok(Ordering::GT)
			} else if self.0 < s {
				Ok(Ordering::LT)
			} else {
				Ok(Ordering::EQ)
			}
		} else {
			make_err!(
				"Can't compare a String with a {}, try casting with 'ToString'",
				other.get_type()
			)
		}
	}

	fn to_float(&self) -> Result<FloatVar, Box<dyn Error>> {
		match self.0.parse() {
			Ok(f) => Ok(FloatVar(f)),
			Err(e) => Err(Box::new(e)),
		}
	}

	fn to_int(&self) -> Result<IntVar, Box<dyn Error>> {
		match self.0.parse() {
			Ok(i) => Ok(IntVar(i)),
			Err(e) => Err(Box::new(e)),
		}
	}

	fn to_string(&self) -> Result<StringVar, Box<dyn Error>> {
		Ok(StringVar(self.0.clone()))
	}
}
