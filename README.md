# Story Rwandan

Port of Story Nigerian from Pascal to Rust.

## Current compatibility

I think it runs ADS2014 perfectly, but there is a bunch of other stuff I
should look into.

## Bundled scripts

- DatingSim.txt (and its credits file dscredits.txt)
  - The good old Andris Dating Simulator 2014
- test.txt
  - Not a game really, just something I used to test comments and random

## How to run

```sh
cargo run -- $SCRIPT_NAME
```

So, to play ADS2014, just do `cargo run -- DatingSim.txt`.

## Other stuff

`hooks` folder contains my `pre-commit` hook, which differs from the
`git` sample only with the addition of `cargo fmt`.  If you want to use it, get
`rustfmt` via `rustup component add rustfmt` and place the script in
`.git/hooks`.
